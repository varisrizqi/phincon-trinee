package com.tipiz.phincon_trinee.utils

import android.content.Context
import java.lang.IllegalArgumentException

class SharedPreferencesHelper (context: Context) {
    //database lokal namanya "myPrefs"
    private val sharedPreferences = context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE)

    fun setValue(key: String, value: Any?) {
        with(sharedPreferences.edit()) {
            when (value) {
                is String -> putString(key, value)
                is Int -> putInt(key, value)
                is Boolean -> putBoolean(key, value)
                is Float -> putFloat(key, value)
                is Long -> putLong(key, value)
                else -> throw IllegalArgumentException("Invalid type of sharedPreferences")
            }
                .apply()
        }
    }

    fun setBoolean(key:String, value:Boolean){
        with(sharedPreferences.edit()){
            putBoolean(key,value).apply()
        }

    }

    fun getBoolean(key:String,defaultValue:Boolean = false):Boolean{
        return sharedPreferences.getBoolean(key,defaultValue)
    }

    fun getValue(key: String, defautlValue: Any? = null): Any? {
        with(sharedPreferences) {
            return when (defautlValue) {
                is String -> getString(key, defautlValue)
                is Int -> getInt(key, defautlValue)
                is Boolean -> getBoolean(key, defautlValue)
                is Float -> getFloat(key, defautlValue)
                is Long -> getLong(key, defautlValue)
                else -> null
            }
        }
    }


    fun removeValue(key: String) {
        sharedPreferences.edit().remove(key).apply()
    }

    fun clearAll() {
        sharedPreferences.edit().clear().apply()
    }
}