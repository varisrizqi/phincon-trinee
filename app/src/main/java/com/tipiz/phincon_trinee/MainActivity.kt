package com.tipiz.phincon_trinee

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.commit
import com.tipiz.phincon_trinee.databinding.ActivityMainBinding
import com.tipiz.phincon_trinee.ui.onboarding.OnBoardingFragment
import com.tipiz.phincon_trinee.ui.splashscreen.SplashScreenFragment

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val splashScreenFragment = SplashScreenFragment()
        val fragmentManager = supportFragmentManager
        val fragment = fragmentManager.findFragmentByTag(SplashScreenFragment::class.java.simpleName)

        if (fragment !is OnBoardingFragment) {
            Log.e("MyflexibleFragment", "Fragment Name :" + SplashScreenFragment::class.java.simpleName)
            fragmentManager.commit {
                add(R.id.frame_container, splashScreenFragment, SplashScreenFragment::class.java.simpleName)
            }
        }
    }
}