package com.tipiz.phincon_trinee.ui.splashscreen

import android.animation.ValueAnimator
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.tipiz.phincon_trinee.R
import androidx.fragment.app.commit
import com.tipiz.phincon_trinee.databinding.FragmentSplashScreenBinding
import com.tipiz.phincon_trinee.ui.login.LoginFragment
import com.tipiz.phincon_trinee.ui.onboarding.OnBoardingFragment
import com.tipiz.phincon_trinee.utils.SharedPreferencesHelper


class SplashScreenFragment : Fragment() {

    private var sharedPreferencesHelper: SharedPreferencesHelper? = null
    private lateinit var binding: FragmentSplashScreenBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View{
        binding = FragmentSplashScreenBinding.inflate(layoutInflater, container, false)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sharedPreferencesHelper = context?.let {
            SharedPreferencesHelper(it)
        }
//        window.setFlags(
//            WindowManager.LayoutParams.FLAG_FULLSCREEN,
//            WindowManager.LayoutParams.FLAG_FULLSCREEN
//        )


//        Handler().postDelayed({
//            startActivity(Intent(this, MainActivity::class.java))
//            finish()
//        }, 2000)

        binding.run {
            animatedView(constraintContainer, FADE_ANIMATION, 0F, 1F, 1000L)
            animatedView(splashCompany, ALPHA_ANIMATION, (0.5).toFloat(), 1F, 800L)
            animatedView(splashYellowContainer, TRANSLATION_ANIMATION, 0F, -45F, 1000L)
            animatedView(splashYellowContainer, ROTATE_ANIMATION, 0F, -15F, 1000L)
            animatedView(splashRedContainer, TRANSLATION_ANIMATION, 0F, 40F, 1000L)
            animatedView(splashRedContainer, ROTATE_ANIMATION, 0F, 10F, 1000L)
            animatedView(splashGreenContainer, "margin_anim", 0F, 300F, 1000L)

            val new = sharedPreferencesHelper?.getBoolean(OnBoardingFragment.KEY_SPLASH,false)

            val p = sharedPreferencesHelper?.getValue(
                OnBoardingFragment.onBoardingValidationKey,
                false
            )



            val isOnBoardingShow2 = if (new == true ){
                new
            } else{
                false
            }



            Handler(Looper.getMainLooper()).postDelayed({
                val loginFragment = LoginFragment()
                val onBoardingFragment = OnBoardingFragment()
                val fragmentManager = parentFragmentManager

                println("varis: $isOnBoardingShow2")

                if (isOnBoardingShow2) {
                    fragmentManager.commit {
                        replace(
                            R.id.frame_container,
                            loginFragment,
                            LoginFragment::class.java.simpleName
                        )
                    }

                } else {
                    fragmentManager.commit {
                        replace(
                            R.id.frame_container,
                            onBoardingFragment,
                            OnBoardingFragment::class.java.simpleName
                        )
                    }
                }
            }, 2000)


        }


    }

    private fun animatedView(
        view: View,
        propertyName: String,
        startValue: Float,
        endValue: Float,
        durationAnimation: Long = 1500L
    ) {
        val animator = ValueAnimator.ofFloat(startValue, endValue)
        animator.duration = durationAnimation
        animator.addUpdateListener { animation ->
            val animatedValue = animation.animatedValue as Float
            view.run {
                when {
                    propertyName.equals(ROTATE_ANIMATION, true) -> {
                        rotation = animatedValue
                    }

                    propertyName.equals(TRANSLATION_ANIMATION, true) -> {
                        translationX = animatedValue
                    }

                    propertyName.equals(ALPHA_ANIMATION, true) -> {
                        scaleX = animatedValue
                    }

                    propertyName.equals(FADE_ANIMATION, true) -> {
                        alpha = animatedValue
                    }

                    else -> {
                        val lp = layoutParams as ConstraintLayout.LayoutParams
                        lp.setMargins(0, 0, 0, animatedValue.toInt())
                        layoutParams = lp
                        requestLayout()

                    }
                }
            }
        }
        animator.start()
    }


    companion object {
        const val ALPHA_ANIMATION = "alpha_animation_key"
        const val ROTATE_ANIMATION = "alpha_animation_key"
        const val TRANSLATION_ANIMATION = "alpha_animation_key"
        const val FADE_ANIMATION = "alpha_animation_key"
    }
}

