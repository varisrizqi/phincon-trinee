package com.tipiz.phincon_trinee.ui.onboarding

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.tipiz.phincon_trinee.R
import com.tipiz.phincon_trinee.databinding.FragmentOnBoardingBinding
import com.tipiz.phincon_trinee.ui.data.SectionsPagerLayout
import com.tipiz.phincon_trinee.ui.login.LoginFragment
import com.tipiz.phincon_trinee.ui.login.RegisterFragment
import com.tipiz.phincon_trinee.utils.SharedPreferencesHelper


class OnBoardingFragment : Fragment() {
    private lateinit var binding: FragmentOnBoardingBinding
    private var sharedPreferencesHelper: SharedPreferencesHelper? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentOnBoardingBinding.inflate(layoutInflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sharedPreferencesHelper = context?.let {
            SharedPreferencesHelper(it)
        }


        val sectionsPagerAdapter = SectionsPagerLayout(requireActivity())
        binding.viewPager.adapter = sectionsPagerAdapter

        binding.tabLayout
        //menghubungkan ViewPager2 dengan TabLayout dengan menggunakan TabLayoutMediator
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position -> }.attach()

//        supportActionBar?.elevation = 0f

        (requireActivity() as AppCompatActivity).supportActionBar?.elevation = 0f


        binding.viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                // Set visibility of btnRight based on the position
                binding.btnRight.visibility = if (position == sectionsPagerAdapter.itemCount - 1) {
                    View.GONE
                } else {
                    View.VISIBLE
                }
            }
        })
        binding.btnRight.setOnClickListener {
            // Move to the next fragment if not at the last fragment
            if (binding.viewPager.currentItem < sectionsPagerAdapter.itemCount - 1) {
                binding.viewPager.currentItem += 1
            }
        }

        binding.btnJoin.setOnClickListener {
            val registerFragment = RegisterFragment()
            val fragmentManager = parentFragmentManager
            fragmentManager.commit {
                replace(
                    R.id.frame_container,
                    registerFragment,
                    RegisterFragment::class.java.simpleName
                )
            }
        }

        binding.btnLeft.setOnClickListener {
            val loginFragment = LoginFragment()
            val fragmentManager = parentFragmentManager

//            sharedPreferencesHelper?.setValue(onBoardingValidationKey,true)
            sharedPreferencesHelper?.setBoolean(KEY_SPLASH,true)
            fragmentManager.commit {
                replace(
                    R.id.frame_container,
                    loginFragment,
                    LoginFragment::class.java.simpleName
                )
            }


        }

    }

    companion object{
        const val KEY_SPLASH= "key_splash"
        const val onBoardingValidationKey = "is_already_show_onboarding"
    }

}
