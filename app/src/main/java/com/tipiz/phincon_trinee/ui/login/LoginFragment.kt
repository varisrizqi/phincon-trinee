package com.tipiz.phincon_trinee.ui.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.commit
import com.tipiz.phincon_trinee.R
import com.tipiz.phincon_trinee.databinding.FragmentLoginBinding
import com.tipiz.phincon_trinee.ui.bottomnav.DashboardFragment
import com.tipiz.phincon_trinee.ui.bottomnav.HomeFragment
import com.tipiz.phincon_trinee.ui.data.TextWatcherConfiguration
import java.util.regex.Pattern

class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(layoutInflater, container, false)
        // Inflate the layout for this fragment
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        validateLogin()


        binding.btnDaftar.setOnClickListener {
            val registerFragment = RegisterFragment()
            val fragmentManager = parentFragmentManager

            fragmentManager.commit {
                replace(
                    R.id.frame_container,
                    registerFragment,
                    RegisterFragment::class.java.simpleName
                )
            }
        }

        binding.btnLogin.setOnClickListener {
//            validateEmpty()
            val dashboardFragment = DashboardFragment()
            val fragmentManager = parentFragmentManager

            fragmentManager.commit {
                replace(
                    R.id.frame_container,
                    dashboardFragment,
                    DashboardFragment::class.java.simpleName
                )
            }
        }
    }

    private fun validateEmpty(): Boolean {
        val email = binding.edEmail.text.toString().trim()
        val password = binding.edPassword.text.toString().trim()
        if (email.isEmpty() && password.isEmpty()) {
            binding.edEmail.error = "Email tidak boleh kosong"
            binding.edPassword.error = "Password tidak boleh kosong"
            return false
        }
        return true
    }

    private fun validateLogin() {
        binding.edEmail.addTextChangedListener(TextWatcherConfiguration(5) {
            isValidEmail(it)
        })

        binding.edPassword.addTextChangedListener(TextWatcherConfiguration(5) {
            isValidPassword(it)
        })
    }


    private fun isValidEmail(email: String) {
        val emailPattern = Pattern.compile("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+")

        if (emailPattern.matcher(email).matches() || email.length <= 5) {
            binding.inputEmail.isErrorEnabled = false
            binding.inputEmail.isHelperTextEnabled = true
            binding.inputEmail.helperText = getString(R.string.contoh_test_gmail_com)
        } else {
            binding.inputEmail.error = getString(R.string.email_tidak_valid)
            binding.inputEmail.isHelperTextEnabled = false
        }
    }

    private fun isValidPassword(password: String) {
        val passwordPattern1 = Pattern.compile(
            "^(?=.*[A-Z])" +
                    "(?=.*[a-z])" +
                    "(?=.*\\d)" +
                    "(?=.*[@#\$%^&+=!])" +
                    "[A-Za-z\\d@#\$%^&+=!]{8,}"
        )
        val passwordPattern = Pattern.compile("(?=.*[A-Za-z\\d@#\$%^&+=!]{8,})")

        if (passwordPattern1.matcher(password).matches() || password.length <= 5) {
            binding.inputPassword.isErrorEnabled = false
            binding.inputPassword.isHelperTextEnabled = true
            binding.inputPassword.helperText = getString(R.string.minimal_8_karakter)
        } else {
            binding.inputPassword.error = getString(R.string.password_tidak_valid)
            binding.inputPassword.isHelperTextEnabled = true
        }
    }
}