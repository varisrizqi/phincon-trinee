package com.tipiz.phincon_trinee.ui.data

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.tipiz.phincon_trinee.ui.onboarding.FirstScreenFragment
import com.tipiz.phincon_trinee.ui.onboarding.SecondScreenFragment
import com.tipiz.phincon_trinee.ui.onboarding.ThreeScreenFragment

class SectionsPagerLayout(activity: FragmentActivity) : FragmentStateAdapter(activity) {
    override fun getItemCount(): Int {
        return 3
    }

    override fun createFragment(position: Int): Fragment {
        var fragment: Fragment? = null

        when (position) {
            0 -> fragment = FirstScreenFragment()
            1 -> fragment = SecondScreenFragment()
            2 -> fragment = ThreeScreenFragment()
        }
        return fragment as Fragment
    }

}