package com.tipiz.phincon_trinee.ui.bottomnav

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.commit
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.NavHostFragment.Companion.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.tipiz.phincon_trinee.R
import com.tipiz.phincon_trinee.databinding.FragmentDashboardBinding
import com.tipiz.phincon_trinee.ui.chart.ChartFragment


class DashboardFragment : Fragment() {

    private lateinit var binding: FragmentDashboardBinding

    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDashboardBinding.inflate(layoutInflater, container, false)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        /*val navView: BottomNavigationView = binding.bottomNav
        val navController = findNavController(R.id.nav_bottom_fragment)
        navView.setupWithNavController(navController)*/

        val navView: BottomNavigationView = binding.bottomNav
        val navHostFragment =
            childFragmentManager.findFragmentById(R.id.nav_bottom_fragment) as NavHostFragment
        navController = navHostFragment.navController

        navView.setupWithNavController(navController)

        binding.toolbar.inflateMenu(R.menu.menu_dashboard)
        val shopping = binding.toolbar.menu
        val actionShopping = shopping.findItem(R.id.action_shopping)
        actionShopping.setOnMenuItemClickListener {
//            navController.navigate(R.id.action_navigation_home_to_chartFragment)
//            true
//            it.findNavController().navigate(R.id.action_navigation_home_to_chartFragment)
            val chartFragment = ChartFragment()
            val fragmentManager = parentFragmentManager

            fragmentManager.commit {
                replace(R.id.frame_container, chartFragment, ChartFragment::class.java.simpleName)
                addToBackStack(null)
            }
            true

        }

    }


}