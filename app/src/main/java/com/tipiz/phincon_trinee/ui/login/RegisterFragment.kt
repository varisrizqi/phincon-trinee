package com.tipiz.phincon_trinee.ui.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import com.tipiz.phincon_trinee.R
import com.tipiz.phincon_trinee.databinding.FragmentRegisterBinding


class RegisterFragment : Fragment() {


    private lateinit var binding: FragmentRegisterBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegisterBinding.inflate(layoutInflater, container, false)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Handle back button press
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            // Perform the fragment transaction to navigate back to LoginFragment
            navigateToLoginFragment()
        }

        binding.imgProfil.setOnClickListener {
            val optionalProfileImageFragment = OptionalProfileImageFragment()

            // memunculkan poop-up/dialog fragment menggunakan "childFragmentManager"
            val fragmentManager = childFragmentManager
            optionalProfileImageFragment.show(fragmentManager, OptionalProfileImageFragment::class.java.simpleName)
        }
    }

    private fun navigateToLoginFragment() {
        // Create a new instance of LoginFragment
        val loginFragment = LoginFragment()

        // Perform fragment transaction to replace current fragment with LoginFragment
        parentFragmentManager.beginTransaction()
            .replace(R.id.frame_container, loginFragment)
//            .addToBackStack(null)
            .commit()
    }


}