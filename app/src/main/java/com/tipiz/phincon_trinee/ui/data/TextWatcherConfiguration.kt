package com.tipiz.phincon_trinee.ui.data

import android.text.Editable
import android.text.TextWatcher

class TextWatcherConfiguration(
    private val characterLimit: Int,
    private val onTextChange: (String) -> Unit
) : TextWatcher {
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        if (s.length >= characterLimit) {
            onTextChange.invoke(s.toString())
        }
    }

    override fun afterTextChanged(s: Editable?) {}
}